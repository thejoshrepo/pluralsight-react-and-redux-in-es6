import React from 'react';

class AboutPage extends React.Component { //Could be stateless functional but is a class due to hot reloading issues
    render() {
        return (
            <div>
                <h1>About</h1>
                <p>This application uses React, Redux, React Router and a variety of other helpful libraries</p>
            </div>
        );
    }
}

export default AboutPage;