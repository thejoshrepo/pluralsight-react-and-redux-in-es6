//CONTAINER COMPONENT (smart component)
import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';
import CourseList from './CourseList';
import {browserHistory} from 'react-router';

/*
 *  Container Structure
 *      1. Constructor
 *          - Initialize state
 *          - Bind functions to the 'this' context
 *      2. Child functions called by render
 *      3. Render (contains child components
 *      4. PropTypes
 *      5. Redux 'connect' and related functions
 *          - mapStateToProps: injects data into the props (courtesy of redux)
 *          - mapDispatchToProps: binds action creators to dispatch function
 */

class CoursesPage extends React.Component {

    constructor(props, context){
        super(props, context);
        this.redirectToAddCoursePage = this.redirectToAddCoursePage.bind(this);
    }

    courseRow(course, index){
        return <div key={index}>{course.title}</div>;
    }

    redirectToAddCoursePage(){
        browserHistory.push('/course');
    }

    render() {
        const {courses} = this.props;
        return (
            <div>
                <h1>Courses</h1>
                <input type="submit"
                       value="Add Course"
                       className="btn btn-primary"
                       onClick={this.redirectToAddCoursePage}/>
                <CourseList courses={courses} />
            </div>
        );
    }
}

CoursesPage.propTypes = {
    courses: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        courses: state.courses //determined by choice made in root reducer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch) //finds all action creators in courseActions.js and wraps a call to dispatch around them
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CoursesPage);