//APPLICATION'S ENTRY POINT

import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import {loadCourses} from './actions/courseActions'; //named import
import {loadAuthors} from './actions/authorActions';
import './styles/styles.css'; //webpack can do this import too
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

const store = configureStore(); //rehydrate store here
store.dispatch(loadCourses()); //calls the action `loadCourses()` from `courseActions` on app start
store.dispatch(loadAuthors());

//final application entry point
render(
    <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
    </Provider>,
    document.getElementById('app')
);